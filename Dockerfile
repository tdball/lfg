FROM gitlab/gitlab-runner

ENV RUSTUP_HOME=/app/rustup
ENV CARGO_HOME=/app/cargo
ENV PATH=/app/cargo/bin:/app/rustup/bin$PATH

RUN apt update -y
RUN apt install gcc -y
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
RUN chown -R gitlab-runner: /app