![Build](https://gitlab.com/tdball/lfg/badges/main/pipeline.svg)
![Release](https://gitlab.com/tdball/lfg/-/badges/release.svg)


# Usage

```
lfg --player NewPlayer --skill Ranged
```

This will create save files the first time it's launched. These are what allow the game to track your progress in the background.

Save files should be located in `$HOME/.config/lfg` or `%AppData%\.config\lfg`
