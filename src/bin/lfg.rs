use clap::Parser;

use lfg::client::game::Game;


#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    #[arg(long)]
    skill: Option<String>,

    #[arg(long)]
    player: String,

    #[arg(short, long)]
    verbose: bool,

    #[arg(short, long)]
    party_passphrase: Option<String>
}

fn main() {
    let args = Args::parse();
    let mut game = Game::load(&args.player);
    if let Some(skill_name) = args.skill {
        game.player.set_active_skill(skill_name)
    }
    if let Some(_party_passphrase) = args.party_passphrase {
        // game.party.lookup_members(party_passphrase)
    }

    game.update();
    game.save();
    game.display(args.verbose);
}
