use std::error::Error;

use lfg::server::daemon::Daemon;

pub fn main() -> Result<(), Box<dyn Error>> {
    let daemon = Daemon::new();
    daemon.start()?;
    Ok(())
}
