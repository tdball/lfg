use std::env;
use std::fs;
use std::path::PathBuf;
use serde::{Deserialize, Serialize};


use crate::party::Party;
use crate::player::Player;
use crate::server::request::{Endpoint, Operation, Request};
use super::message::Message;

#[derive(Deserialize, Serialize, Debug)]
pub struct Game {
    #[serde(default)]
    pub party: Party,
    pub player: Player,
}

impl Game {
    pub fn get_save_file_path(player_name: &str) -> PathBuf {
        let home = env::var("HOME").expect("$HOME should be set");
        PathBuf::from(format!("{}/.config/lfg/{}.lfgsave.toml", home, player_name))
    }

    fn new(player: Player) -> Self {
        // Should only be called if we can't load a game for the player from the server
        Game {player, party: Party::default()}
    }

    pub fn create_player(player_name: &str) {
        let player = Player::new(player_name.to_string());
        let data = toml::to_string(&player).expect("Should serialize player");
        let mut message = Message::from(Request::from((Endpoint::PLAYER, Operation::CREATE, data)));
        message.send();
        println!("{:?}", message.response)
    }

    fn load_player(player_name: &str) {
        let mut message = Message::from(Request::from((Endpoint::PLAYER, Operation::READ, player_name.to_string())));
        message.send();
        if let Some(response) = message.response {
            println!("{:?}", response)
        }
    }

    pub fn load(player_name: &str) -> Game {
        Game::load_player(player_name);
        // Game::load_from_server(player_name);
        Game::create_player(player_name);
        match fs::read_to_string(Game::get_save_file_path(player_name)) {
            Ok(data) => {
                let game: Game = toml::from_str(&data).expect("Save file corrupted");
                game
            },
            Err(_e) => {
                // error case is fine, file doesn't exist, we'll generate it
                panic!("Unable to load game");
            }
        }
    }

    pub fn update(&mut self) {
        // connect to server, send update data, wait and store locally
        let activity = self.player.get_active_skill_name();
        self.player.set_active_skill(String::from(activity));
        self.player.update_exp();
    }

    pub fn display(&self, verbose: bool) {
        if verbose {
            self.player.show_all()
        } else {
            self.player.show()
        }
    }

    pub fn save(&self) {
        let data = match toml::ser::to_string(self) {
            Ok(data) => data,
            Err(e) => panic!("Error: {e:?}")
        };

        let save_file = Game::get_save_file_path(&self.player.name);
        if let Some(directory) = save_file.parent() {
            fs::create_dir_all(directory).expect("Parent directory should exist");
        }
        match fs::write(Game::get_save_file_path(&self.player.name), data) {
            Ok(_) => {},
            Err(e) => panic!("Error: {e:?}")
        };
    }
}



