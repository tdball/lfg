use std::{io::{Read, Write}, net::Shutdown, os::unix::net::UnixStream};

use serde::{Deserialize, Serialize};

use crate::{config::socket_path, server::{request::Request, response::Response}};

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Message {
    pub request: Option<Request>,
    pub response: Option<Response>,
}

impl Message {
    pub fn new(request: Option<Request>, response: Option<Response>) -> Message {
        Message {request, response}
    }
    pub fn empty() -> Message {
        Message::new(None, None)
    }
    pub fn send(&mut self) {
        let mut stream = UnixStream::connect(socket_path()).expect("Should be able to connect to lfgd.sock");
        let payload = toml::to_string(&self).expect("Message should serialize");
        stream.write_all(payload.as_bytes()).unwrap();

        // Required for read_to_string methods to function
        stream.shutdown(Shutdown::Write).unwrap();

        let mut buffer = String::new();
        stream.read_to_string(&mut buffer).unwrap();
        let response: Response = toml::from_str(&buffer).expect("Should be Response object from server");
        if let Some(error) = &response.error {
            println!("{error}");
        }
        self.response = Some(response);
    }
}

impl From<Request> for Message {
    fn from(request: Request) -> Message {
        Message {
            request: Some(request),
            response: None
        }
    }
}
impl From<Response> for Message {
    fn from(response: Response) -> Message {
        Message {
            request: None,
            response: Some(response)
        }
    }
}
