use std::{env, path::PathBuf};


pub fn socket_path() -> PathBuf {
    let home = env::var("HOME").expect("$HOME should be set");
    PathBuf::from(format!("{}/.config/lfg/lfgd.sock", home))
}