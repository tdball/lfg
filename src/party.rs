use std::collections::BTreeMap;

use serde::{Deserialize, Serialize};

use crate::player::Player;

#[derive(Deserialize, Serialize, Debug)]

pub struct Party {
    pub players: BTreeMap<String, Player>
}
impl Default for Party {
    fn default() -> Party {
        Party::new()
    }
}
impl Party {
    pub fn new() -> Party {
        Party {
            players: BTreeMap::default()
        }
    }
    pub fn add_member(&mut self, player: Player) {
        self.players.insert(player.name.to_string(), player);
    }
}