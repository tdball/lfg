use std::{collections::BTreeMap, time::Duration};

use serde::{Deserialize, Serialize};

use crate::server::utils;

use super::client::message::Message;

use super::skill::Skill;
use super::server::request::{Request, Endpoint, Operation};
use super::server::skill_table::SkillRecord;
use super::server::table::TableCollection;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Player {
    pub name: String,
    pub skills: BTreeMap<String, Skill>,
    active_skill_name: Option<String>,
    last_active: Duration,
}

impl Player {
    fn skills() -> BTreeMap<String, Skill> {
        BTreeMap::from([
            ("Woodcutting".into(), Skill::new("Woodcutting".into(), 3)),
            ("Slayer".into(), Skill::new("Slayer".into(), 3)),
            ("Attack".into(), Skill::new("Attack".into(), 3)),
            ("Ranged".into(), Skill::new("Ranged".into(), 3)),
            ("Magic".into(), Skill::new("Magic".into(), 3)),
        ])
    }

    pub fn get_skills_from_server() {
        let request = Request::from((Endpoint::SKILL, Operation::LIST));
        let mut message = Message::from(request);
        message.send();
        if let Some(response) = message.response {
            if let Some(data) = response.data {
                let items: TableCollection<SkillRecord> = toml::from_str(&data).expect("Response data should be TableCollection<SkillEntry>");
                println!("{:?}", items);
            }
        }
    }

    pub fn new(name: String) -> Self {
        Player {
            name,
            skills: Player::skills(),
            active_skill_name: None,
            last_active: utils::get_system_time(),
        }
    }

    pub fn training_new_skill(&self, name: &str) -> bool {
        if let Some(active_skill_name) = &self.active_skill_name {
            return name != active_skill_name;
        }
        false

    }

    pub fn update_exp(&mut self) {
        let elapsed_time = utils::get_system_time() - self.last_active;
        if let Some(skill_name) = &self.active_skill_name {
            if let Some(skill) = self.skills.get_mut(skill_name) {
                let gained_exp = skill.calculate_gained_exp(&elapsed_time);
                skill.update(gained_exp);
                self.last_active = utils::get_system_time()
            }
        }
    }

    pub fn set_active_skill(&mut self, name: String) {
        // Player::get_skills_from_server();
        if self.training_new_skill(&name) {
            // Set last_active to current time as we're
            // training a new skill; Without this we would
            // accidentally give a burst of exp to the skill
            // being switched to
            self.last_active = utils::get_system_time();
        }
        self.active_skill_name = Some(name);
    }

    pub fn get_active_skill_name(&self) -> &str {
        if let Some(active_skill_name) = &self.active_skill_name {
            active_skill_name.as_str()
        } else {
            "Idling"
        }
    }

    pub fn show(&self) {
        let skill_name = self.get_active_skill_name();
        if let Some(skill) = self.skills.get(skill_name) {
            println!("Training - {}", skill)
        }
    }
    pub fn show_all(&self) {
        for skill in self.skills.values() {
            println!("{skill}");
        }
        self.show();
    }
}