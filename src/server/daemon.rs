use std::error::Error;
use std::io::{Read, Write};
use std::os::unix::net::{UnixListener, UnixStream};
use std::sync::{Arc, Mutex};
use std::{fs, thread};

use crate::client::message::Message;

use crate::config::socket_path;

use super::database::Database;
use super::router::Router;
pub struct Daemon {
    pub database: Arc<Mutex<Database>>,
    pub socket_listener: UnixListener,
}

impl Default for Daemon {
    fn default() -> Self {
        Self::new()
    }
}

impl Daemon {
    pub fn new() -> Daemon {
        let db = Database::load();
        Daemon {
            database: Arc::new(Mutex::new(db)),
            socket_listener: Daemon::init_socket(),
        }
    }

    pub fn client_handler(mut stream: UnixStream, database: Arc<Mutex<Database>>) {
        let mut buffer = String::new();
        stream.read_to_string(&mut buffer).unwrap();
        let message: Message = toml::from_str(&buffer).expect("Request should be properly formatted");
        
        match database.try_lock() {
            Ok(db) => {
                let mut router = Router::new(db);
                if let Some(request) = message.request {
                    let response = match router.route(request) {
                        Ok(message) => message,
                        Err(error) => panic!("{error}")
                    };
                    let response_payload = toml::to_string(&response).unwrap();
                    stream.write_all(response_payload.as_bytes()).unwrap();
                }
            },
            Err(error) => {
                println!("Error: {error}");
                println!("Database lock poisoned, clearing");
                database.clear_poison()
            }
        }

        match database.try_lock() {
            Ok(db) => {
                db.dump();
            },
            Err(error) => {
                println!("Error: {error}");
                println!("Unable to save database");
            }

        }

    }

    fn init_socket() -> UnixListener {
        let socket_path = socket_path();
        match fs::remove_file(&socket_path){
            Ok(_) => {
                println!("Leftover socket exists, cleaning")
            },
            Err(_) => {
                println!("No Socket currently in use, starting server")
            },
        }
        if let Some(parent_dir) = socket_path.parent() {
            fs::create_dir_all(parent_dir).expect("Should be able to create config dir for lfg");
        }
        match UnixListener::bind(socket_path) {
            Ok(listener) => listener,
            Err(error) => {
                panic!("Error: {error}")
            },
        }
    }

    pub fn start(&self) -> Result<(), Box<dyn Error>> {
        for stream in self.socket_listener.incoming() {
            match stream {
                Ok(stream) => {
                    /* connection succeeded */
                    let db = Arc::clone(&self.database);
                    thread::spawn(move || Daemon::client_handler(stream, db));
                }
                Err(err) => {
                    /* connection failed */
                    println!("{err}");
                    break;
                }
            }
        }
        Ok(())
    }
}
