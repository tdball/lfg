
use std::{env, fs, path::PathBuf};

use serde::{Deserialize, Serialize};

use crate::skill::Skill;

use super::{player_table::PlayerTable, skill_table::SkillTable};

#[derive(Serialize, Deserialize)]
pub struct Database {
    // Find a better way to do this
    pub skill: SkillTable,
    pub player: PlayerTable,
}

impl Default for Database {
    fn default() -> Self {
        Self::new()
    }
}

impl Database {
    pub fn new() -> Database {
        Database {
            player: PlayerTable::new(),
            skill: SkillTable::from(vec![
                Skill::new("Woodcutting".into(), 3),
                Skill::new("Slayer".into(), 3),
                Skill::new("Attack".into(), 3),
                Skill::new("Ranged".into(), 3),
                Skill::new("Magic".into(), 3),
            ])
        }
    }
    fn database_file_path() -> PathBuf {
        let home = env::var("HOME").expect("$HOME should be set");
        PathBuf::from(format!("{}/.config/lfg/db.toml", home))
    }
    pub fn dump(&self) {
        let data = toml::ser::to_string(self).expect("Should serialize database to string");
        let file_path = Database::database_file_path();
        if let Some(directory) = file_path.parent() {
            fs::create_dir_all(directory).expect("Parent directory should exist");
        }
        match fs::write(file_path, data) {
            Ok(_) => {},
            Err(e) => panic!("Error: {e:?}")
        };
    }

    pub fn load() -> Database {
        let file_path = Database::database_file_path();
        if !file_path.exists() {
            println!("No existing database, creating new");
            return Database::new();
        }
        match fs::read_to_string(file_path) {
            Ok(data) => {
                println!("Attempting to load existing database");
                let database: Database = toml::from_str(&data).expect("Database corrupted");
                database
            },
            Err(_e) => {
                panic!("Unable to load database");
            }
        }
    }
}