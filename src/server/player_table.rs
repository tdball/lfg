use std::{collections::BTreeMap, time::Duration};

use serde::{Deserialize, Serialize};

use crate::player::Player;

use super::{table::{ErrorKind, TableCollection, TableError, CRUDL}, utils};

#[derive(Serialize, Deserialize)]
pub struct PlayerTable {
    table: BTreeMap<String, PlayerRecord>
}

impl Default for PlayerTable {
    fn default() -> Self {
        Self::new()
    }
}
impl PlayerTable {
    pub fn new() -> PlayerTable {
        PlayerTable {
            table: BTreeMap::default()
        }
    }
}
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct PlayerRecord {
    pub created_at: Duration,
    pub updated_at: Duration,
    pub player: Player,
}

impl PlayerRecord {
    pub fn new(player: Player) -> PlayerRecord {
        PlayerRecord {
            player,
            created_at: utils::get_system_time(),
            updated_at: utils::get_system_time(),
        }
    }
}

impl CRUDL<Player> for PlayerTable {
    fn create(&mut self, data: Player) -> Result<(), TableError> {
        if self.table.contains_key(&data.name) {
            let error = TableError::new(ErrorKind::RecordExists, format!("Player {} already exists", &data.name));
            Err(error)
        } else {
            let record = PlayerRecord::new(data);
            self.table.insert(record.player.name.to_string(), record);
            Ok(())
        }
    }

    fn read(&self, identifier: String) -> Option<&Player> {
        if let Some(record) = self.table.get(&identifier) {
            return Some(&record.player)
        }
        None
    }

    fn list(&self) -> TableCollection<Player> {
        // Gotta be a better way to do this than to just clone
        let items: Vec<Player> = self.table.clone()
            .into_values()
            .map(|record| {record.player})
            .collect();
        TableCollection{items}
    }

    fn update(&mut self, identifier: String, data: Player) {
        let mut existing_record: PlayerRecord;
        if let Some(record) = self.table.get_mut(&data.name) {
            existing_record = record.clone();
        } else {
            println!("No record for {identifier}");
            return
        }
        existing_record.updated_at = utils::get_system_time();
        self.table.insert(identifier, existing_record);
    }

    fn delete(&mut self, identifier: String) {
        self.table.remove_entry(&identifier);
    }
}