use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Request {
    pub endpoint: Endpoint,
    pub operation: Operation,
    pub data: Option<String>,
}

impl From<(Endpoint, Operation)> for Request {
    fn from((endpoint, operation): (Endpoint, Operation)) -> Request {
        Request { endpoint, operation, data: None }
    }
}
impl From<(Endpoint, Operation, String)> for Request {
    fn from((endpoint, operation, data): (Endpoint, Operation, String)) -> Request {
        Request { endpoint, operation, data: Some(data) }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum Operation {
    CREATE,
    READ,
    UPDATE,
    DELETE,
    LIST,

}
#[derive(Serialize, Deserialize, Eq, Hash, PartialEq, Clone, Debug)]
pub enum Endpoint {
    SKILL,
    PLAYER,
}