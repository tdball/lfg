use serde::{Deserialize, Serialize};


#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Response {
    pub data: Option<String>,
    pub error: Option<String>,
}

impl Response {
    pub fn success(data: &str) -> Response {
        Response {
            data: Some(data.to_string()),
            error: None
        }
    }
    pub fn error(detail: &str) -> Response {
        Response {
            data: None,
            error: Some(detail.to_string())
        }
    }
}