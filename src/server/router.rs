use std::sync::MutexGuard;

use toml::de::Error;


use crate::player::Player;

use super::{database::Database, request::{Endpoint, Operation, Request}, response::Response, table::ErrorKind};

use super::table::CRUDL;

pub struct Router<'a> {
    database: MutexGuard<'a, Database>
}


impl Router<'_> {
    pub fn new(database: MutexGuard<'_, Database>) -> Router {
        Router{database}
    }
    pub fn route(&mut self, request: Request) -> Result<Response, Error> {
        println!("Handling operation {:?} for endpoint {:?}", &request.operation, &request.endpoint);
        match request.endpoint {
            Endpoint::PLAYER => self.player_handler(request.operation, request.data),
            Endpoint::SKILL => self.skill_handler(request.operation, request.data),
        }
    }
    fn player_handler(&mut self, operation: Operation, data: Option<String>) -> Result<Response, Error> {
        match operation {
            Operation::CREATE | Operation::UPDATE => {
                if let Some(data) = data {
                    let player: Player = toml::from_str(&data).expect("Should deserialize into Player");
                    match self.database.player.create(player) {
                        Ok(_) => {
                            return Ok(Response::success("Created player successfully"))
                        },
                        Err(error) => {
                            if error.kind == ErrorKind::RecordExists {
                                return Ok(Response::error(&error.message))
                            }
                        }
                    };
                }
                Ok(Response::error("Unable to create player"))
            },
            Operation::READ => {
                if let Some(data) = data {
                    let player = self.database.player.read(data);
                    if let Some(player) = player {
                        let response_payload = toml::to_string(player).expect("Should Serialize into PlayerEntry");
                        return Ok(Response::success(&response_payload))
                    }
                }
                Ok(Response::error("Unable to find player"))
            },
            _ => {todo!()}
        }
    }

    fn skill_handler(&self, operation: Operation, _data: Option<String>) -> Result<Response, Error> {
        match operation {
            Operation::LIST => {
                let entries = self.database.skill.list();
                let response_data = toml::to_string(&entries).unwrap();
                Ok(Response::success(&response_data))
            },
            _ => {
                Ok(Response::error("Unsupported operation"))
            }
        }
    }
}