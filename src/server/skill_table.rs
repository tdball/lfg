use std::collections::BTreeMap;


use serde::{Deserialize, Serialize};


use crate::skill::Skill;

use super::table::{ErrorKind, TableCollection, TableError, CRUDL};

#[derive(Serialize, Deserialize)]
pub struct SkillTable {
    table: BTreeMap<String, SkillRecord>
}

impl Default for SkillTable {
    fn default() -> Self {
        Self::new()
    }
}

impl SkillTable {
    pub fn new() -> SkillTable {
        SkillTable {
            table: BTreeMap::default()
        }
    }
}

impl From<Vec<Skill>> for SkillTable {
    fn from(value: Vec<Skill>) -> SkillTable {
        let mut skill_table = SkillTable::new();
        for item in value {
            skill_table.create(item);
        }
        skill_table
    }
}


#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct SkillRecord {
    pub skill: Skill,
}

impl SkillRecord {
    pub fn new(skill: Skill) -> SkillRecord {
        SkillRecord {
            skill
        }
    }
}


impl CRUDL<Skill> for SkillTable {
    fn create(&mut self, data: Skill) -> Result<(), TableError> {
        if self.table.contains_key(&data.name) {
            let error = TableError::new(ErrorKind::RecordExists, format!("Skill {} already exists", &data.name));
            Err(error)
        } else {
            let record = SkillRecord::new(data);
            self.table.insert(record.skill.name.to_string(), record);
            Ok(())
        }
    }

    fn read(&self, identifier: String) -> Option<&Skill> {
        if let Some(record) = self.table.get(&identifier) {
            return Some(&record.skill)
        }
        None
    }

    fn list(&self) -> TableCollection<Skill> {
        // Gotta be a better way to do this than to just clone
        let items: Vec<Skill> = self.table.clone()
            .into_values()
            .map(|record| {record.skill})
            .collect();
        TableCollection{items}
    }

    fn update(&mut self, identifier: String, data: Skill) {
        self.table.insert(identifier, SkillRecord::new(data));
    }

    fn delete(&mut self, identifier: String) {
        self.table.remove_entry(&identifier);
    }
}