use std::{fmt::Display};

use serde::{Deserialize, Serialize};


#[derive(Serialize, Deserialize, Debug)]
pub struct TableCollection<T> {
    pub items: Vec<T>
}

pub trait CRUDL<Data> {
    fn create(&mut self, data: Data) -> Result<(), TableError>;    
    fn read(&self, identifier: String) -> Option<&Data>;
    fn list(&self) -> TableCollection<Data>;
    fn update(&mut self, identifier: String, data: Data);
    fn delete(&mut self, identifier: String);
}

#[derive(Debug)]
pub struct TableError {
    pub kind: ErrorKind,
    pub message: String
}
impl TableError {
    pub fn new(kind: ErrorKind, message: String) -> TableError {
        TableError{
            kind,
            message,
        }
    }
}


impl Display for TableError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.message)
    }   
}

#[derive(Debug, PartialEq)]
pub enum ErrorKind {
    RecordExists
}
