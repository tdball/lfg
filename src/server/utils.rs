use std::time::{Duration, SystemTime, UNIX_EPOCH};

pub fn get_system_time() -> Duration {
    SystemTime::now().duration_since(UNIX_EPOCH).expect("System time should be available")
}