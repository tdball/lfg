use std::{fmt::Display, time::Duration};

use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Skill {
    pub name: String,
    pub level: u64,
    pub experience: u64,
    pub experience_rate: u64,
}

impl Skill {
    pub fn new(name: String, experience_rate: u64) -> Self {
        Skill {
            name,
            level: 1,
            experience: 0,
            experience_rate,
        }
    }

    pub fn calculate_gained_exp(&self, elapsed_time: &Duration) -> u64 {
        self.experience_rate * elapsed_time.as_secs()
    } 

    pub fn calculate_level(experience: u64) -> u64 {
        const EXP_DIVISION_CONSTANT: u64 = 83;
        1 + (experience / EXP_DIVISION_CONSTANT)
    }
    pub fn update(&mut self, gained_experience: u64) {
        self.experience += gained_experience;
        self.level = Skill::calculate_level(self.experience);
    }
}

impl Display for Skill {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Skill: {}, Level: {}, Total Experience: {}", self.name, self.level, self.experience)
    }
}