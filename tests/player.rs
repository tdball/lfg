
#[cfg(test)]
mod player {
    use std::time::Duration;
    use lfg::player::Player;

    #[test]
    fn set_active_skill_updates() {
        let mut player = Player::new(String::from("test_player"));
        assert_eq!(player.get_active_skill_name(), "Idling");
        player.set_active_skill(String::from("Woodcutting"));
        assert_eq!(player.get_active_skill_name(), "Woodcutting")
    }

    #[test]
    fn training_new_skill() {
        let mut player = Player::new(String::from("test_player"));
        player.set_active_skill("Woodcutting".into());
        assert!(player.training_new_skill("Slayer"));
    }

    #[test]
    fn training_same_skill() {
        let mut player = Player::new(String::from("test_player"));
        player.set_active_skill("Woodcutting".into());
        assert!(!player.training_new_skill("Woodcutting"));
    }

    #[test]
    fn training_gains_experience() {
        let player = Player::new(String::from("test_player"));
        if let Some(skill) = player.skills.get("Woodcutting") {
            let elapsed_time = Duration::from_secs(3);
            let expected_exp = elapsed_time.as_secs() * skill.experience_rate;
            let gained_exp = skill.calculate_gained_exp(&elapsed_time);
            assert_eq!(gained_exp, expected_exp)
        }
    }

    #[test]
    fn training_new_skill_does_not_carry_over() {
        let mut player = Player::new(String::from("test_player"));
        player.set_active_skill("Woodcutting".into());
        if let Some(skill) = player.skills.get_mut("Woodcutting") {
            let gained = skill.calculate_gained_exp(&Duration::from_secs(3));
            skill.update(gained);
        }
        player.set_active_skill("Slayer".into());
        if let Some(skill) = player.skills.get_mut("Slayer") {
            let gained = skill.calculate_gained_exp(&Duration::from_secs(3));
            skill.update(gained);
        }
    }
}